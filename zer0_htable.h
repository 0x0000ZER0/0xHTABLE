#ifndef ZER0_HTABLE_H
#define ZER0_HTABLE_H

#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>

typedef struct {
	void   *key;
	size_t  key_len;
	void   *val;
	size_t  val_len;
} zer0_htable_pair;

typedef struct {
	uint32_t   cap;
	uint32_t   len;
	bool      *busy;
	void     **keys;
	void     **vals;	
} zer0_htable;

void
zer0_htable_init(zer0_htable*, size_t, size_t);

uint32_t
zer0_htable_hash(void*, size_t, uint32_t);

bool
zer0_htable_cmp(void*, void*, size_t);

void
zer0_htable_push(zer0_htable*, zer0_htable_pair*);

void*
zer0_htable_find(zer0_htable*, void*, size_t, size_t);

void
zer0_htable_remove(zer0_htable*, void*, size_t);

void
zer0_htable_free(zer0_htable*);

#endif