# 0xHTABLE

A generic hash table using `void*` and customizable `hash` and `compare` functions.

### API

- Creating the hash table.

```c
zer0_htable htable;
zer0_htable_init(&htable, sizeof (int), sizeof (double));
//...
zer0_htable_free(&htable);
```

- Pushing an item to it.

```c
int    key;
double val;

key = 197;
val = 10.0;

zer0_htable_pair pair;
pair.key     = (void*)&key;
pair.key_len = sizeof (key);
pair.val     = (void*)&val;
pair.val_len = sizeof (val);

zer0_htable_push(&htable, &pair);
```

- Searching for an item given the key.

```c
int    key;
void  *val;

key = 197;

val = zer0_htable_find(&htable, (void*)&key, sizeof (int), sizeof (double));

printf("%f\n", *(double*)val);
```

- Removing the item given the key.

```c
int    key;
void  *val;

key = 197;

zer0_htable_remove(&htable, (void*)&key, sizeof (int));
```

- Customizing the `hash` and `compare` functions. For that you need to override the macros in `zer0_htable_defs.h` file.

```c
#define ZER0_HTABLE_HASH(key, len, cap) MY_CUSTOM_HASH_FUNCTION(key, len, cap);
#define ZER0_HTABLE_CMP(key1, key2, len) MY_CUSTOM_CMP_FUNCTION(key1, key2, len);
```

