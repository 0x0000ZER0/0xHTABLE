#ifndef ZER0_HTABLE_DEFS_H
#define ZER0_HTABLE_DEFS_H

#define ZER0_HTABLE_HASH(key, len, cap) zer0_htable_hash(key, len, cap);
#define ZER0_HTABLE_CMP(key1, key2, len) zer0_htable_cmp(key1, key2, len);

#endif