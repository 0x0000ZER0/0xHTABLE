#include "zer0_htable.h"
#include "zer0_htable_defs.h"

#include <malloc.h>
#include <string.h>

#ifdef ZER0_DEBUG
#include <stdio.h>
#endif

void
zer0_htable_init(zer0_htable *htable, size_t key_len, size_t val_len) {
	htable->cap  = 4;
	htable->len  = 0;
	htable->busy = calloc(htable->cap, sizeof (bool));
	htable->keys = malloc(htable->cap * key_len);
	htable->vals = malloc(htable->cap * val_len);
}


uint32_t
zer0_htable_hash(void *key, size_t len, uint32_t cap) {
	uint8_t *ptr;
	ptr = (uint8_t*)key;

	uint32_t res;
	uint32_t i;

	res = 0;
	i   = 0; 
	while (i < (uint32_t)len) {
		res += ptr[i];
		++i;
	}

	res = (res - len) % cap;

	return res;
}


bool
zer0_htable_cmp(void *key1, void *key2, size_t key_len) {
	return memcmp(key1, key2, key_len);
}

void
zer0_htable_push(zer0_htable *htable, zer0_htable_pair *pair) {
	if (htable->len == htable->cap) {
		size_t old_cap;
		old_cap = htable->cap;

		htable->cap  *= 2;
		htable->busy  = realloc(htable->busy, htable->cap * sizeof (bool));
		htable->keys  = realloc(htable->keys, htable->cap * pair->key_len);
		htable->vals  = realloc(htable->vals, htable->cap * pair->val_len);

		memset(htable->busy + old_cap, false, old_cap * sizeof (bool));
	}

	uint32_t  idx;

	size_t atm;
	atm = 0;
	do  {
		idx = ZER0_HTABLE_HASH(pair->key, pair->key_len + atm, htable->cap);
		++atm;
	} while (htable->busy[idx] == true);

#ifdef ZER0_DEBUG
	printf("ZER0 HTABLE DEBUG: IDX = %u | ATM = %zu\n", idx, atm);
#endif
	htable->busy[idx] = true;

	void *ptr;

	ptr = htable->keys + idx * pair->key_len;
	memcpy(ptr, pair->key, pair->key_len);

	ptr = htable->vals + idx * pair->val_len;
	memcpy(ptr, pair->val, pair->val_len);

	++htable->len;
}

void*
zer0_htable_find(zer0_htable *htable, void *key, size_t key_len, size_t val_len) {
	uint32_t  idx;
	bool      cmp;
	void     *h_key; 

	size_t atm;
	atm = 0;
	do  {
		idx = ZER0_HTABLE_HASH(key, key_len + atm, htable->cap);
		++atm;

		h_key = htable->keys + idx * key_len;

		cmp = ZER0_HTABLE_CMP(key, h_key, key_len);

#ifdef ZER0_DEBUG
	printf("ZER0 HTABLE DEBUG: IDX = %u | ATM = %zu\n", idx, atm);
#endif
	} while (cmp == false);

	void *h_val;
	h_val = htable->vals + idx * val_len;

	return h_val;
}

void
zer0_htable_remove(zer0_htable *htable, void *key, size_t key_len) {
	uint32_t  idx;
	bool      cmp;
	void     *h_key; 

	size_t atm;
	atm = 0;
	do  {
		idx = ZER0_HTABLE_HASH(key, key_len + atm, htable->cap);
		++atm;

		h_key = htable->keys + idx * key_len;

		cmp = ZER0_HTABLE_CMP(key, h_key, key_len);

#ifdef ZER0_DEBUG
	printf("ZER0 HTABLE DEBUG: IDX = %u | ATM = %zu\n", idx, atm);
#endif
	} while (cmp == false);

	htable->busy[idx] = false;
}

void
zer0_htable_free(zer0_htable *htable) {
	free(htable->busy);
	free(htable->vals);
	free(htable->keys);
}
